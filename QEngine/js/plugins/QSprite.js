var Imported = Imported || {}
Imported.QSprite = "2.1.9"
var QPlus = QPlus || {}

if (!Imported.QPlus || !QPlus.versionCheck(Imported.QPlus, "1.4.0")) {
  alert("Error: QSprite requires QPlus 1.4.0 or newer to work.")
  throw new Error("Error: QSprite requires QPlus 1.4.0 or newer to work.")
}

/*:
 * @plugindesc <QSprite>
 * Lets you configure Spritesheets
 * @author Quxios  | Version 2.1.9
 * @target MZ
 *
 * @requires QPlus
 *
 * @param File Name Identifier
 * @desc Set the file name identifier for QSprites
 * Default: %{config}-
 * @default %{config}-
 *
 * @param Random Idle Interval
 * @text Random Idle Interval
 * @desc Set the time interval between random Idles (in frames)
 * @type struct<Range>
 * @default {"Min":"30","Max":"600"}
 *
 * @param Use New Adjust
 * @desc Use new pose speed adjust?
 * @type boolean
 * @on Yes
 * @off No
 * @default true
 *
 * @help
 * ============================================================================
 * ## About
 * ============================================================================
 * This plugin lets you use sprites that are set up with QSprite Editor
 *
 * https://github.com/quxios/QSpriteEditor
 *
 * ============================================================================
 * ## How to use
 * ============================================================================
 * First configure your sprite with the QSprite Editor. Then you can use your
 * sprites by identifying it as a QSprite. To do so, just name your sprite file
 * by using the File Name Identifier format. By default this is:
 * ~~~
 *  %{config}-
 * ~~~
 * You would replace {config} with the config you made inside the QSprite
 * Editor. For example, if I made a config named: `Hero` then I would name
 * the file something like: `%Hero-Example.png`
 *
 * ============================================================================
 * ## Built-in Poses
 * ============================================================================
 * This plugin adds a few built in poses:
 * - moveX
 * - dashX
 * - idleX
 * - idle[A-Z]X ( more info for this below )
 * - default
 *
 * Where X is the direction:
 * - 2: down
 * - 4: left
 * - 6: right
 * - 8: up
 * - 1: lower left
 * - 3: lower right
 * - 7: upper left
 * - 9: upper right
 *
 * **_(Diagonals only work if you are using this with QMovement)_**
 *
 * Default pose is used when and idleX or moveX is not found. Note that default
 * does not have an X at the end, it's just default. Has no directions tied to
 * it.
 *
 * ----------------------------------------------------------------------------
 * **idle[A-Z]X**
 * ----------------------------------------------------------------------------
 * This is a random idle that will play a random `idle[A-Z]` every X frames.
 * The random wait depends on the `Random Idle Interval` parameter. To clarify
 * you won't be naming this pose `idle[A-Z]2` (for example for the down direction)
 * you would name it `idleA2` or `idleB2` or `idleC2`, ect.
 *
 * You can also add in a multipier if you want one of the idles to appear more
 * often then others by adding: `Tx` Where T is the multipler.
 *
 * For example:
 *
 * Lets say I want 4 `idle[A-Z]` poses, and I want one of them to have a 4 times
 * better chance of appearing then the rest. My idle names would be:
 *
 * - idleA2
 * - idleB2
 * - idleC2
 * - idleD4x2
 *
 * **_Note: all their directions are 2(down)_**
 * 
 * ============================================================================
 * ## Notetags / Comments
 * ============================================================================
 * **Set default direction**
 * ----------------------------------------------------------------------------
 * With spritesheets being large, it may be hard to pick that events starting
 * direction. To fix that, you can add a comment in that event that will set
 * it's default direction.
 * ~~~
 *  <direction:X>
 * ~~~
 * Set X to direction. 2 for down, 4 left, 6 right, 8 up.
 
 * ============================================================================
 * ## Plugin Commands
 * ============================================================================
 * **Playing a Pose**
 * ----------------------------------------------------------------------------
 * Play a pose.
 * ~~~
 *  qSprite [CHARAID] play [POSE] [list of options]
 * ~~~
 * - CHARAID: The character identifier.
 *  - For player: 0, p, or player
 *  - For events: EVENTID, eEVENTID, eventEVENTID or this for the event that called this
 *  (replace EVENTID with a number)
 * - POSE: The pose to play (Don’t add the direction! ex: atk, not atk2)
 *
 * Possible options:
 * - lock: Disable character movement while pose is playing
 * - pause: Pause the pose on the last frame
 * - breakable: If character moves, the pose will end
 * - wait: Next Event Command runs once pose is complete
 *
 * ----------------------------------------------------------------------------
 * **Looping a Pose**
 * ----------------------------------------------------------------------------
 * Loop a pose until it's cleared, broken out of or played over.
 * ~~~
 *  qSprite [CHARAID] loop [POSE] [list of options]
 * ~~~
 * - CHARAID: The character identifier.
 *  - For player: 0, p, or player
 *  - For events: EVENTID, eEVENTID, eventEVENTID or this for the event that called this
 *  (replace EVENTID with a number)
 * - POSE: The pose to play (Don’t add the direction! ex: atk, not atk2)
 *
 * Possible options:
 * - lock: Disable character movement while pose is playing
 * - breakable: If character moves, the loop will end
 * - wait: Next Event Command runs once first loop has is complete
 * 
 * ----------------------------------------------------------------------------
 * **Clearing**
 * ----------------------------------------------------------------------------
 * Clear current playing/looping pose.
 * ~~~
 *  qSprite [CHARAID] clear
 * ~~~
 * - CHARAID: The character identifier.
 *  - For player: 0, p, or player
 *  - For events: EVENTID, eEVENTID, eventEVENTID or this for the event that called this
 *  (replace EVENTID with a number)
 * 
 * ----------------------------------------------------------------------------
 * **Add / Remove an idle[A-Z]**
 * ----------------------------------------------------------------------------
 * Maybe you only want to play an idle[A-Z] during certain scenes. So you can
 * add and remove them whenever you want!
 * ~~~
 *  qSprite [CHARAID] addIdleAZ [POSE]
 *
 *  qSprite [CHARAID] removeIdleAZ [POSE]
 * ~~~
 * - CHARAID: The character identifier.
 *  - For player: 0, p, or player
 *  - For events: EVENTID, eEVENTID, eventEVENTID or this for the event that called this
 *  (replace EVENTID with a number)
 * - POSE: The idle[A-Z] to add (Don’t add the direction! ex: idleA not idleA2)
 * 
 * ----------------------------------------------------------------------------
 * **Change idle pose**
 * ----------------------------------------------------------------------------
 * Maybe you want to have a different idle for certain parts of the game. I
 * would recommend just using a different spritesheet, but I added a plugin
 * command to let you change your idle!
 * 
 * ~~~
 *  qSprite [CHARAID] changeIdle [POSE]
 * ~~~
 * - CHARAID: The character identifier.
 *  - For player: 0, p, or player
 *  - For events: EVENTID, eEVENTID, eventEVENTID or this for the event that called this
 * - POSE: The new pose to use when idle (Don’t add the direction! ex: idleA not idleA2)
 * 
 * ----------------------------------------------------------------------------
 * **Examples**
 * ----------------------------------------------------------------------------
 * ~~~
 *  qSprite 0 play confused pause breakable
 *  qSprite p play confused pause breakable
 *  qSprite player play confused pause breakable
 * ~~~
 * *Note: All 3 are the same, just using a different character id method*
 *
 * The player will run the `confused` pose. The pose will stop on the last frame.
 * Once the player moves, the pose will end. The player can move during this pose
 * and the next event command will run immediatly after this command with no
 * wait.
 *
 * ~~~
 *  qSprite 1 play hug wait
 *  qSprite e1 play hug wait
 *  qSprite event1 play hug wait
 * ~~~
 * Event 1 will run the hug pose. The event can't move until the pose is
 * complete, and the next event command will run once the pose is complete.
 * 
 * ============================================================================
 * ## Links
 * ============================================================================
 * Formated Help:
 *
 *  https://quxios.github.io/#/plugins/QSprite
 *
 * RPGMakerWebs:
 *
 *  http://forums.rpgmakerweb.com/index.php?threads/qplugins.73023/
 *
 * Terms of use:
 *
 *  https://github.com/quxios/QMV-Master-Demo/blob/master/readme.md
 *
 * Like my plugins? Support me on Patreon!
 *
 *  https://www.patreon.com/quxios
 *
 * @tags character, sprite, animation
 *
 * @command play
 * @desc
 *
 * @arg pose
 * @type string
 * @desc The pose to play (Don’t add the direction! ex: atk, not atk2)* 
 * @default 
 *
 * @arg charId
 * @desc The character identifier.
 *  - For player: 0, p, or player 
 *  - For events: EVENTID, eEVENTID, eventEVENTID or this for the event that called this
 * @type string 
 * @default ""
 *
 * @arg options
 * @type struct<CommandOptions>
 * @default {"lock":"false","pause":"false","breakable":"false","wait":"false"}
 *
 * @command clear
 * @desc
 *
 * @command loop
 * @desc
 *
 * @arg pose
 * @type string
 * @desc The pose to play (Don’t add the direction! ex: atk, not atk2)* 
 * @default 
 *
 * @arg charId
 * @desc The character identifier.
 *  - For player: 0, p, or player
 *  - For events: EVENTID, eEVENTID, eventEVENTID or this for the event that called this
 * @type string 
 * @default ""
 *
 * @arg options
 * @type struct<CommandOptions>
 *
 * @command clear
 * @desc Clear current playing/looping pose.
 * @arg charId
 * @desc The character identifier.
 *  - For player: 0, p, or player
 *  - For events: EVENTID, eEVENTID, eventEVENTID or this for the event that called this
 * @type string
 * @default ""
 **
 *
 * @command addidleaz
 * @desc Maybe you only want to play an idle[A-Z] during certain scenes. So you can add and remove them whenever you want!
 *
 * @arg pose
 * @type string
 * @desc The pose to play (Don’t add the direction! ex: atk, not atk2)*
 * @default
 *
 * @arg charId
 * @desc The character identifier.
 *  - For player: 0, p, or player
 *  - For events: EVENTID, eEVENTID, eventEVENTID or this for the event that called this
 * @type string
 * @default ""
 *
 * @command removeidleaz
 * @desc Maybe you only want to play an idle[A-Z] during certain scenes. So you can add and remove them whenever you want!
 *
 * @arg pose
 * @type string
 * @desc The pose to play (Don’t add the direction! ex: atk, not atk2)*
 * @default
 *
 * @arg charId
 * @desc The character identifier.
 *  - For player: 0, p, or player
 *  - For events: EVENTID, eEVENTID, eventEVENTID or this for the event that called this
 * @type string
 * @default ""

 * @command changeidle
 * @desc Maybe you want to have a different idle for certain parts of the game. I would recommend just using a different spritesheet, but I added a plugin command to let you change your idle!
 *
 * @arg pose
 * @type string
 * @desc The pose to play (Don’t add the direction! ex: atk, not atk2)*
 * @default
 *
 * @arg charId
 * @desc The character identifier.
 *  - For player: 0, p, or player
 *  - For events: EVENTID, eEVENTID, eventEVENTID or this for the event that called this
 * @type string
 * @default ""
 */

/*~struct~CommandOptions:
 *
 * @param lock
 * @desc Disable character movement while pose is playing
 * @type boolean
 * @default false
 *
 * @param pause
 * @desc Pause the pose on the last frame
 * @type boolean
 * @default false
 *
 * @param breakable
 * @desc If character moves, the pose will end
 * @type boolean
 * @default false
 *
 * @param wait
 * @desc Next Event Command runs once pose is complete
 * @type boolean
 * @default false
 */

/*~struct~Range:
 *
 * @param Min
 * @type number
 * @desc IDs will be automatically set for each map in the order that they are created.
 * @min 1
 * @default 30
 *
 * @param Max
 * @desc The name of the map event.
 * @type number
 * @min 1
 * @default 600
 *
 */

//=============================================================================

//=============================================================================
// QSprite Static Class

function QSprite() {
  throw new Error("This is a static class")
}
QSprite.json = null;

//=============================================================================
// QSprite
(() => {
  "use strict"

  const pluginParams = $plugins.filter((i) => {
    return i.description.contains("<QSprite>")
  })

  const pluginName = pluginParams.length > 0 && pluginParams[0].name

  let _PARAMS = pluginParams[0].parameters
  let _IDENTIFIER = _PARAMS["File Name Identifier"] || "%{config}-"

  _IDENTIFIER = _IDENTIFIER.replace("{config}", "(.+?)")
  _IDENTIFIER = new RegExp(_IDENTIFIER)

  //for debug
  QSprite._IDENTIFIER = _IDENTIFIER

  const _IDLEINTERVAL = [
    _PARAMS["Random Idle Interval"].Min,
    _PARAMS["Random Idle Interval"].Max,
  ]

  if (_IDLEINTERVAL[1] < _IDLEINTERVAL[0]) {
    _IDLEINTERVAL[1] = _IDLEINTERVAL[0]
  }

  const _USENEWADJUST = _PARAMS["Use New Adjust"]
  const _HASQMOVEMENT = Imported.Quasi_Movement || Imported.QMovement

  QPlus.request("data/QSprite.json")
    .onSuccess(function (data) {
      QSprite.json = data
    })
    .onError(function () {
      alert("Error:" + this.url + " not found.")
      QSprite.json = {}
    })

  const Alias_Scene_Base_isReady = Scene_Base.prototype.isReady

  Scene_Base.prototype.isReady = function () {
    return Alias_Scene_Base_isReady.call(this) && QSprite.json
  }

  //-----------------------------------------------------------------------------
  // Game_CharacterBase

  const Alias_Game_CharacterBase_initMembers =
    Game_CharacterBase.prototype.initMembers

  Game_CharacterBase.prototype.initMembers = function () {
    Alias_Game_CharacterBase_initMembers.call(this)
    this._pose = ""
    this._idlePose = "idle"
    this._availableIdlePoses = []
    this._idleTimer = 0
    this._idleIntervalWait = Math.randomIntBetween(
      _IDLEINTERVAL[0],
      _IDLEINTERVAL[1]
    )
  }

  Game_CharacterBase.prototype.moveSpeedMultiplier = function () {
    const ds = 4 - this.realMoveSpeed()
    return Math.pow(2, ds)
  }

  const Alias_Game_CharacterBase_animationWait =
    Game_CharacterBase.prototype.animationWait

  Game_CharacterBase.prototype.animationWait = function () {
    if (this.qSprite() && this.qSprite().poses[this._pose]) {
      const pose = this.qSprite().poses[this._pose]
      if (pose.adjust) {
        if (_USENEWADJUST) {
          return pose.speed / this.moveSpeedMultiplier()
        } else {
          return (pose.speed - this.realMoveSpeed()) * 3
        }
      }
      return pose.speed
    }
    return Alias_Game_CharacterBase_animationWait.call(this)
  }

  Game_CharacterBase.prototype.calcPoseWait = function () {
    if (!this.qSprite()) return 0
    const frameWait = this.animationWait()
    const frames = this.qSprite().poses[this._pose].pattern.length
    return Math.ceil(frameWait * frames)
  }

  const Alias_Game_CharacterBase_update = Game_CharacterBase.prototype.update

  Game_CharacterBase.prototype.update = function () {
    const wasMoving = this.isMoving()
    Alias_Game_CharacterBase_update.call(this)
    if (this.qSprite()) {
      this.updatePose(wasMoving)
    } else {
      this._pose = ""
    }
  }

  Game_CharacterBase.prototype.isDiagonal = function () {
    return false;
  }

  Game_CharacterBase.prototype.updatePose = function (wasMoving) {
    const diag = this.isDiagonal()
    const isMoving = wasMoving || this.isMoving()
    if (this._posePlaying) {
      if (!this._posePlaying.canBreak) return
      if (!isMoving) return
      this.clearPose()
    }
    const dir = this._direction
    if (_HASQMOVEMENT && this.isDiagonal()) {
    }
    if (!isMoving && this.hasPose(this._idlePose + dir)) {
      this.updateIdlePose(dir, diag)
    } else {
      this.updateSteppingPose(isMoving, wasMoving)
      if (this._posePlaying) return
      this.updateMovingPose(dir, diag, isMoving)
    }
    if (this._pose === "") this._pose = "default"
  }

  Game_CharacterBase.prototype.updateIdlePose = function (dir, diag) {
    if (diag && this.hasPose(this._idlePose + diag)) {
      dir = diag
    }
    if (this._pose !== this._idlePose + dir) {
      this._pattern = 0
      this._animationCount = 0
      this._isIdle = true
    }
    this._pose = this._idlePose + dir
    this.updateIdleInterval()
  }

  Game_CharacterBase.prototype.updateSteppingPose = function (
    isMoving,
    wasMoving
  ) {
    this._isIdle = !isMoving
    if (this._isIdle && wasMoving) {
      this._pattern = 0
    } else if (this._isIdle) {
      if (!this.hasStepAnime()) {
        this._pattern = 0
      }
      this.updateIdleInterval()
    } else if (!this._isIdle) {
      this._idleTimer = 0
      this._idleIntervalWait = Math.randomIntBetween(
        _IDLEINTERVAL[0],
        _IDLEINTERVAL[1]
      )
    }
  }

  Game_CharacterBase.prototype.updateIdleInterval = function () {
    this._idleTimer++
    if (this._availableIdlePoses.length > 0) {
      if (this._idleTimer >= this._idleIntervalWait) {
        const i = Math.randomInt(this._availableIdlePoses.length)
        const pose = this._availableIdlePoses[i]
        this.playPose(pose, false, false, false, true)
        this._idleIntervalWait = Math.randomIntBetween(
          _IDLEINTERVAL[0],
          _IDLEINTERVAL[1]
        )
        this._idleTimer = 0
      }
    }
  }

  Game_CharacterBase.prototype.updateDashingPose = function (dir, diag) {
    if (diag && this.hasPose("dash" + diag)) {
      dir = diag
    }
    if (this.hasPose("dash" + dir)) {
      this._pose = "dash" + dir
      return true
    }
    return false
  }

  Game_CharacterBase.prototype.updateMovingPose = function (
    dir,
    diag,
    isMoving
  ) {
    if (this.isDashing() && isMoving) {
      if (this.updateDashingPose(dir, diag)) {
        return
      }
    }
    if (diag && this.hasPose("move" + diag)) {
      dir = diag
    }
    if (this.hasPose("move" + dir)) {
      this._pose = "move" + dir
    }
  }

  const Alias_Game_CharacterBase_updateAnimationCount =
    Game_CharacterBase.prototype.updateAnimationCount

  Game_CharacterBase.prototype.updateAnimationCount = function () {
    if (this._isIdle || this._posePlaying) {
      this._animationCount++
      return
    }
    Alias_Game_CharacterBase_updateAnimationCount.call(this)
  }

  const Alias_Game_CharacterBase_updatePattern =
    Game_CharacterBase.prototype.updatePattern

  Game_CharacterBase.prototype.updatePattern = function () {
    if (this._isIdle || this._posePlaying || this.qSprite()) {
      this._pattern++
      if (this._pattern >= this.maxPattern()) {
        if (this._posePlaying) {
          if (this._posePlaying.pause) {
            this._pattern--
            return
          }
          if (!this._posePlaying.loop) {
            this.clearPose()
            return
          }
        }
        this.resetPattern()
      }
      return
    }
    return Alias_Game_CharacterBase_updatePattern.call(this)
  }

  const Alias_Game_CharacterBase_maxPattern =
    Game_CharacterBase.prototype.maxPattern

  Game_CharacterBase.prototype.maxPattern = function () {
    if (this.qSprite()) {
      const pose = this.qSprite().poses[this._pose]
      return pose ? pose.pattern.length : 0
    }
    return Alias_Game_CharacterBase_maxPattern.call(this)
  }

  Game_CharacterBase.prototype.resetPattern = function () {
    this.qSprite() ? this.setPattern(0) : this.setPattern(1)
  }

  const Alias_Game_CharacterBase_straighten =
    Game_CharacterBase.prototype.straighten

  Game_CharacterBase.prototype.straighten = function () {
    const oldAnimCount = this._animationCount
    const oldPattern = this._pattern
    Alias_Game_CharacterBase_straighten.call(this)
    if (this.qSprite() && (this.hasWalkAnime() || this.hasStepAnime())) {
      this._pattern = 0
    }
    if (this.qSprite() && this._posePlaying) {
      this._animationCount = oldAnimCount
      this._pattern = oldPattern
    }
  }

  Game_CharacterBase.prototype.hasPose = function (pose) {
    if (this.qSprite()) {
      return this.qSprite().poses.hasOwnProperty(pose)
    }
    return false
  }

  const Alias_Game_CharacterBase_setImage =
    Game_CharacterBase.prototype.setImage
  Game_CharacterBase.prototype.setImage = function (
    characterName,
    characterIndex
  ) {
    const wasPosePlaying = this._posePlaying
    Alias_Game_CharacterBase_setImage.call(this, characterName, characterIndex)
    this._isQChara = null
    this._isIdle = null
    this._posePlaying = null
    this.getAvailableIdlePoses()
    if (this.isQCharacter()) {
      this._posePlaying = wasPosePlaying
    }
  }

  Game_CharacterBase.prototype.getAvailableIdlePoses = function () {
    this._availableIdlePoses = []
    if (this.isQCharacter()) {
      const poses = this.qSprite().poses
      for (const pose in poses) {
        const match = /^idle[a-zA-Z](\d+x)?[12346789]$/.exec(pose)
        if (match) {
          const name = pose.slice(0, -1)
          if (!this._availableIdlePoses.contains(name)) {
            let x = 1
            if (match[1]) {
              x = Number(match[1].slice(0, -1))
            }
            for (let i = 0; i < x; i++) {
              this._availableIdlePoses.push(name)
            }
          }
        }
      }
    }
  }

  Game_CharacterBase.prototype.addRandIdle = function (pose) {
    const match = /^(.*)[a-zA-Z]([0-9]+x)?$/.exec(pose)
    if (match) {
      if (!this._availableIdlePoses.contains(pose)) {
        let x = 1
        if (match[2]) {
          x = Number(match[2].slice(0, -1))
        }
        for (let i = 0; i < x; i++) {
          this._availableIdlePoses.push(pose)
        }
      }
    }
  }

  Game_CharacterBase.prototype.removeRandIdle = function (pose) {
    for (let i = this._availableIdlePoses.length - 1; i >= 0; i--) {
      if (this._availableIdlePoses[i] === pose) {
        this._availableIdlePoses.splice(i, 1)
      }
    }
  }

  Game_CharacterBase.prototype.changeIdle = function (pose) {
    this._idlePose = pose
  }

  Game_CharacterBase.prototype.playPose = function (
    pose,
    lock,
    pause,
    looping,
    canBreak
  ) {
    if (!this.qSprite()) {
      console.error("there is no qSprite")
    }
    let dir = this._direction
    if (_HASQMOVEMENT && this.isDiagonal()) {
      const diag = this.isDiagonal()
      if (this.hasPose(pose + diag)) {
        dir = diag
      }
    }
    if (this.hasPose(pose + dir)) {
      pose += dir
    } else if (!this.hasPose(pose)) {
      console.error(`No pose with name ${pose}`)
      return
    }
    this._pose = pose
    this._posePlaying = {
      lock: lock,
      pause: pause,
      loop: looping,
      canBreak: canBreak,
    }
    this._animationCount = 0
    this._pattern = 0
  }

  Game_CharacterBase.prototype.loopPose = function (pose, lock, canBreak) {
    // function kept for backwards compatibility
    return this.playPose(pose, lock, false, true, canBreak)
  }

  Game_CharacterBase.prototype.clearPose = function () {
    this._pose = ""
    this._posePlaying = null
    this._locked = false
    this._animationCount = 0
    this._pattern = 0
    this.updatePose(false)
  }

  Game_CharacterBase.prototype.isQCharacter = function () {
    if (this._isQChara === null) {
      this._isQChara = this._characterName.match(_IDENTIFIER)
    }
    return this._isQChara ? this._isQChara[1] : false
  }

  Game_CharacterBase.prototype.qSprite = function () {
    return QSprite.json[this.isQCharacter()] || null
  }

  //-----------------------------------------------------------------------------
  // Game_Player

  const Alias_Game_Player_canMove = Game_Player.prototype.canMove
  Game_Player.prototype.canMove = function () {
    if (this._posePlaying && this._posePlaying.lock) return false
    return Alias_Game_Player_canMove.call(this)
  }

  //-----------------------------------------------------------------------------
  // Game_Event

  const Alias_Game_Event_setupPageSettings =
    Game_Event.prototype.setupPageSettings
  Game_Event.prototype.setupPageSettings = function () {
    Alias_Game_Event_setupPageSettings.call(this)
    const match = /<direction:(\d+?)>/i.exec(this.comments())
    if (match) {
      this.setDirection(Number(match[1]))
    }
  }

  //-----------------------------------------------------------------------------
  // Window_Base

  const Alias_Window_Base_drawCharacter = Window_Base.prototype.drawCharacter
  Window_Base.prototype.drawCharacter = function (
    characterName,
    characterIndex,
    x,
    y
  ) {
    let qSprite = characterName.match(_IDENTIFIER)
    if (qSprite) {
      qSprite = QSprite.json[qSprite[1]]
    }
    if (!qSprite) {
      return Alias_Window_Base_drawCharacter.call(
        this,
        characterName,
        characterIndex,
        x,
        y
      )
    }
    const pose =
      qSprite.poses["idle2"] ||
      qSprite.poses["move2"] ||
      qSprite.poses["default"]
    if (!pose) return
    const bitmap = ImageManager.loadCharacter(characterName),
      i = pose.pattern[0] || 0,
      sx = i % qSprite.cols,
      sy = (i - sx) / qSprite.cols
    const h = bitmap.width / qSprite.cols
    const w = bitmap.height / qSprite.rows
    this.contents.blt(bitmap, sx, sy, w, h, x - w / 2, y - h)
  }

  //-----------------------------------------------------------------------------
  // Sprite_Character

  Sprite_Character.prototype.qSprite = function () {
    return this._character.qSprite()
  }

  const Alias_Sprite_Character_updateBitmap =
    Sprite_Character.prototype.updateBitmap

  Sprite_Character.prototype.updateBitmap = function () {
    if (this.isImageChanged()) {
      Alias_Sprite_Character_updateBitmap.call(this)
      const qSprite = this.qSprite()
      if (qSprite) {
        this.anchor.x = qSprite.anchorX || 0.5
        this.anchor.y = qSprite.anchorY || 1
      }
    }
  }

  Sprite_Character.prototype.updateCharacterFrame = function () {
    const pw = this.patternWidth()
    const ph = this.patternHeight()
    const sx = (this.characterBlockX() + this.characterPatternX()) * pw
    const sy = (this.characterBlockY() + this.characterPatternY()) * ph
    this.updateHalfBodySprites()
    if (this._bushDepth > 0) {
      const offsetA = Math.round(ph - ph * this.anchor.y)
      const d = this._bushDepth + offsetA
      this._upperBody.setFrame(sx, sy, pw, ph - d)
      this._lowerBody.setFrame(sx, sy + ph - d, pw, d)
      this.setFrame(sx, sy, 0, ph)
    } else {
      this.setFrame(sx, sy, pw, ph)
    }
  }

  const Alias_Sprite_Character_updateHalfBodySprites =
    Sprite_Character.prototype.updateHalfBodySprites

  Sprite_Character.prototype.updateHalfBodySprites = function () {
    Alias_Sprite_Character_updateHalfBodySprites.call(this)
    if (this._bushDepth > 0) {
      const ph = this.patternHeight()
      const offsetA = Math.round(ph - ph * this.anchor.y)
      const offsetB = ph - offsetA
      this._upperBody.y = -offsetB
      this._lowerBody.y = -this._bushDepth
    }
  }

  const Alias_Sprite_Character_createHalfBodySprites =
    Sprite_Character.prototype.createHalfBodySprites
  Sprite_Character.prototype.createHalfBodySprites = function () {
    const upper = this._upperBody
    const lower = this._lowerBody
    Alias_Sprite_Character_createHalfBodySprites.call(this)
    if (!upper) {
      this._upperBody.anchor.x = this.anchor.x
      this._upperBody.anchor.y = 0
    }
    if (!lower) {
      this._lowerBody.anchor.x = this.anchor.x
      this._lowerBody.anchor.y = 0
      this._lowerBody.opacity = 128
    }
  }

  let Alias_Sprite_Character_characterBlockX =
    Sprite_Character.prototype.characterBlockX

  Sprite_Character.prototype.characterBlockX = function () {
    if (this.qSprite()) return 0
    return Alias_Sprite_Character_characterBlockX.call(this)
  }

  const Alias_Sprite_Character_characterBlockY =
    Sprite_Character.prototype.characterBlockY

  Sprite_Character.prototype.characterBlockY = function () {
    if (this.qSprite()) return 0
    return Alias_Sprite_Character_characterBlockY.call(this)
  }

  const Alias_Sprite_Character_characterPatternX =
    Sprite_Character.prototype.characterPatternX
  Sprite_Character.prototype.characterPatternX = function () {
    const qSprite = this.qSprite()
    if (qSprite) {
      const pose = qSprite.poses[this._character._pose]
      if (!pose) return 0
      const pattern = pose.pattern
      const i = pattern[this._character._pattern]
      const x = i % qSprite.cols
      return x
    }
    return Alias_Sprite_Character_characterPatternX.call(this)
  }

  let Alias_Sprite_Character_characterPatternY =
    Sprite_Character.prototype.characterPatternY

  Sprite_Character.prototype.characterPatternY = function () {
    const qSprite = this.qSprite()
    if (qSprite) {
      const pose = qSprite.poses[this._character._pose]
      if (!pose) return 0
      const pattern = pose.pattern
      const i = pattern[this._character._pattern]
      const x = i % qSprite.cols
      const y = (i - x) / qSprite.cols
      return y
    }
    return Alias_Sprite_Character_characterPatternY.call(this)
  }

  const Alias_Sprite_Character_patternWidth =
    Sprite_Character.prototype.patternWidth

  Sprite_Character.prototype.patternWidth = function () {
    const qSprite = this.qSprite()
    if (qSprite) {
      return this.bitmap.width / qSprite.cols
    }
    return Alias_Sprite_Character_patternWidth.call(this)
  }

  const Alias_Sprite_Character_patternHeight =
    Sprite_Character.prototype.patternHeight

  Sprite_Character.prototype.patternHeight = function () {
    const qSprite = this.qSprite()
    if (qSprite) {
      return this.bitmap.height / qSprite.rows
    }
    return Alias_Sprite_Character_patternHeight.call(this)
  }

  //-----------------------------------------------------------------------------
  // Sprite_Actor

  Sprite_Actor.prototype.isQCharacter = function () {
    if (this._isQChara === undefined) {

      this._isQChara = this._battlerName.match(_IDENTIFIER)
    }
    return this._isQChara ? this._isQChara[1] : false
  }

  const Alias_Sprite_Actor_startMotion = Sprite_Actor.prototype.startMotion

  Sprite_Actor.prototype.startMotion = function (motionType) {
    if (this.isQCharacter()) {
      const pose = motionType
      const motion = this._qSprite.poses[pose]
      if (motion) {
        this._pose = pose
        this._pattern = 0
        this._motionCount = 0
      }
    } else {
      Alias_Sprite_Actor_startMotion.call(this, motionType)
    }
  }

  const Alias_Sprite_Actor_updateBitmap = Sprite_Actor.prototype.updateBitmap
  Sprite_Actor.prototype.updateBitmap = function () {
    const oldBattlerName = this._battlerName
    Alias_Sprite_Actor_updateBitmap.call(this)
    if (oldBattlerName !== this._battlerName) {
      this._isQChara = undefined
      if (this.isQCharacter()) {
        this._qSprite = QSprite.json[this.isQCharacter()]
      }
    }
  }

  const Alias_Sprite_Actor_updateFrame = Sprite_Actor.prototype.updateFrame

  Sprite_Actor.prototype.updateFrame = function () {
    if (this.isQCharacter()) {
      Sprite_Battler.prototype.updateFrame.call(this)
      const bitmap = this._mainSprite.bitmap
      if (bitmap) {
        const motion = this._qSprite.poses[this._pose]
        if (!motion) {
          this._mainSprite.visible = false
          return
        }
        this._mainSprite.visible = true
        const pattern = motion.pattern
        const i = pattern[this._pattern]
        const cw = bitmap.width / this._qSprite.cols
        const ch = bitmap.height / this._qSprite.rows
        const cx = i % this._qSprite.cols
        const cy = (i - cx) / this._qSprite.cols
        this._mainSprite.setFrame(cx * cw, cy * ch, cw, ch)
      }
    } else {
      Alias_Sprite_Actor_updateFrame.call(this)
    }
  }

  const Alias_Sprite_Actor_updateMotionCount =
    Sprite_Actor.prototype.updateMotionCount

  Sprite_Actor.prototype.updateMotionCount = function () {
    if (this.isQCharacter()) {
      const motion = this._qSprite.poses[this._pose]
      if (!motion) return
      const poseWait = motion.speed
      if (++this._motionCount >= poseWait) {
        this._pattern++
        const maxPattern = motion.pattern.length
        if (this._pattern === maxPattern) {
          this.refreshMotion()
        }
        this._motionCount = 0
      }
    } else {
      Alias_Sprite_Actor_updateMotionCount.call(this)
    }
  }

  const Alias_Sprite_Actor_refreshMotion = Sprite_Actor.prototype.refreshMotion

  Sprite_Actor.prototype.refreshMotion = function () {
    if (this.isQCharacter()) {
      const actor = this._actor
      if (actor) {
        const stateMotion = actor.stateMotionIndex()
        if (actor.isInputting()) {
          this.startMotion("idle2")
        } else if (actor.isActing()) {
          this.startMotion("walk")
        } else if (stateMotion === 3) {
          this.startMotion("dead")
        } else if (stateMotion === 2) {
          this.startMotion("sleep")
        } else if (actor.isChanting()) {
          this.startMotion("chant")
        } else if (actor.isGuard() || actor.isGuardWaiting()) {
          this.startMotion("guard")
        } else if (stateMotion === 1) {
          this.startMotion("abnormal")
        } else if (actor.isDying()) {
          this.startMotion("dying")
        } else if (actor.isUndecided()) {
          this.startMotion("idle1")
        } else {
          this.startMotion("idle2")
        }
      }
    } else {
      Alias_Sprite_Actor_refreshMotion.call(this)
    }
  }

  //-----------------------------------------------------------------------------
  // Game_Actor

  const Alias_Game_Actor_performAction = Game_Actor.prototype.performAction
  Game_Actor.prototype.performAction = function (action) {
    Alias_Game_Actor_performAction.call(this, action)
    if (action._item._dataClass === "skill") {
      const id = action._item._itemId
      const skill = $dataSkills[id]
      const motion = skill.meta.motion
      if (motion) {
        this.requestMotion(motion)
      }
    }
  }

  //-----------------------------------------------------------------------------
  // PluginManager

  // commands: play clear loop addidleaz removeidleaz changeidle
  Game_Interpreter.prototype.qSpriteCommand = function (args) {
    let wait
    let canBreak
    let locked
    let pose
    let chara
    if (args[0].toLowerCase() === "this") {
      chara = this.character(0)
    } else {
      chara = QPlus.getCharacter(args[0])
    }
    if (!chara) return
    const cmd = args[1].toLowerCase()
    const args2 = args.slice(2)

    if (cmd === "play") {
      pose = args2.shift()
      locked = !!QPlus.getArg(args2, /^lock$/i)
      const pause = !!QPlus.getArg(args2, /^pause$/i)
      canBreak = !!QPlus.getArg(args2, /^breakable$/i)
      wait = !!QPlus.getArg(args2, /^wait$/i)
      chara.playPose(pose, locked, pause, false, canBreak)
      if (wait) {
        this.wait(chara.calcPoseWait())
      }
    }

    if (cmd === "loop") {
      pose = args2.shift()
      locked = !!QPlus.getArg(args2, /^lock$/i)
      canBreak = !!QPlus.getArg(args2, /^breakable$/i)
      wait = !!QPlus.getArg(args2, /^wait$/i)
      chara.loopPose(pose, locked, canBreak)
      if (wait) {
        this.wait(chara.calcPoseWait())
      }
    }
    if (cmd === "clear") {
      chara.clearPose()
    }
    if (cmd === "addidleaz") {
      chara.addRandIdle(args2[0])
    }
    if (cmd === "removeidleaz") {
      chara.removeRandIdle(args2[0])
    }
    if (cmd === "changeidle") {
      chara.changeIdle(args2[0])
    }
  }

  QSprite.parseCommandArguments = function (args) {
    const { pose, charId, options } = args
    let { wait, breakable, locked, pause } = JSON.parse(options)
    wait = QPlus.convertToBool(wait)
    breakable = QPlus.convertToBool(breakable)
    locked = QPlus.convertToBool(locked)
    pause = QPlus.convertToBool(pause)
    return {
      pose,
      charId,
      options: {
        wait,
        breakable,
        locked,
        pause
      }
    }
  }

  PluginManager.registerCommand(pluginName, "play", (args) => {
    const data = QSprite.parseCommandArguments(args)
    const chara = QPlus.getCharacter(data.charId)
    chara.playPose(data.pose, data.options.locked, data.options.pause, false, data.options.breakable)
    if (data.pose.wait) {
      this.wait(chara.calcPoseWait())
    }
  })

  PluginManager.registerCommand(pluginName, "loop", (args) => {
    const data = QSprite.parseCommandArguments(args)
    const chara = QPlus.getCharacter(data.charId)
    chara.loopPose(data.pose, data.options.locked, data.options.breakable)
    if (data.pose.wait) {
      this.wait(chara.calcPoseWait())
    }
  })

  PluginManager.registerCommand(pluginName, 'clear', (args) => {
    const {charId} = args;
    const chara = QPlus.getCharacter(charId)
    chara.clearPose()
  })

  PluginManager.registerCommand(pluginName, 'addidleaz', (args) => {
    const {pose, charId} = args;
    const chara = QPlus.getCharacter(charId)
    chara.addRandIdle(pose)
  })

  PluginManager.registerCommand(pluginName, 'removeidleaz', (args) => {
    const {pose, charId} = args;
    const chara = QPlus.getCharacter(charId)
    chara.removeRandIdle(pose)
  })

  PluginManager.registerCommand(pluginName, 'changeidle', (args) => {
    const {pose, charId} = args;
    const chara = QPlus.getCharacter(charId)
    chara.changeIdle(pose)
  })

})()
